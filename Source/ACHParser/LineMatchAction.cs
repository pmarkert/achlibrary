﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ephisys.ACH {
    internal class LineMatchAction {
        internal Regex Regex;

        internal LineMatchDelegate Action;
    }
}
