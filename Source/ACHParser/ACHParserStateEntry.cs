﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    internal class ACHParserStateEntry : ACHParserState {
        protected ACHBatch owningBatch;

        internal ACHParserStateEntry(ACHBatch owningBatch, ACHFile file)
            : base(file) {
                this.owningBatch = owningBatch;
        }

        internal override ACHParserState ParseLine(string line) {
            throw new NotImplementedException();
        }

        protected override LineMatchAction[] MatchActions {
            get { throw new NotImplementedException(); }
        }
    }
}
