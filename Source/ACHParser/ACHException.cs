﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    public class ACHException : ApplicationException {
        internal ACHException(string message)
            : base(message) {
        }
    }
}
