﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ephisys.ACH {
    internal abstract class ACHParserState {
        protected ACHFile achFile;

        internal ACHParserState(ACHFile file) {
            this.achFile = file;
        }

        internal virtual ACHParserState ParseLine(string line) {
            foreach (LineMatchAction lineMatchAction in MatchActions) {
                Match m = lineMatchAction.Regex.Match(line);
                if (m.Success) {
                    return lineMatchAction.Action(m);
                }
            }
            ACHParserState nextState = NextState;
            if (nextState != null) {
                return nextState.ParseLine(line);
            }
            else {
                return null;
            }
        }

        protected abstract LineMatchAction[] MatchActions {
            get;
        }

        protected virtual ACHParserState NextState {
            get {
                return null;
            }
        }
    }
}
