﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using log4net;

namespace Ephisys.ACH {
	internal class ACHParserStateNone : ACHParserState {
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


		private Regex reStartOfTransmission = new Regex(@"^\$IDENT\d{2}(?<ProcessingInstitution>\d{9})N$");
		private Regex reACHFileHeader = new Regex(@"^1(?<PriorityCode>\d{2}) (?<DestinationRTN>\d{9}) (?<OriginRTN>\d{9})(?<FileDate>\d{10})(?<FileModifier>[0-9A-Z])094101(?<DestinationName>.{23})(?<OriginName>.{23})(?<ReferenceCode>.{8})$");
		private Regex reACHFileFooter = new Regex(@"^9(?<BatchCount>\d{6})(?<BlockCount>\d{6})(?<EntryAddendaCount>\d{8})(?<EntryHash>\d{10})(?<DebitTotal>\d{12})(?<CreditTotal>\d{12}) {39}$");
		private Regex reEndOfTransmission = new Regex(@"^\$ENDFILE\d{9}N$");

		internal ACHParserStateNone(ACHFile file)
			: base(file) {
		}

		protected override LineMatchAction[] MatchActions {
			get {
				return new LineMatchAction[] {
                    new LineMatchAction() { Regex = reStartOfTransmission, Action = MatchedStartOfTransmission },
                    new LineMatchAction() { Regex = reACHFileHeader, Action = MatchedACHFileHeader },
                    new LineMatchAction() { Regex = reACHFileFooter, Action = MatchedACHFileFooter },
                    new LineMatchAction() { Regex = reEndOfTransmission, Action = MatchedEndOfTransmission }
                };
			}
		}

		internal ACHParserState MatchedStartOfTransmission(Match match) {
			achFile.ProcessingInstitutionFIID = match.Groups["ProcessingInstitution"].Value;
			return this;
		}

		internal ACHParserState MatchedACHFileHeader(Match match) {
			achFile.PriorityCode = int.Parse(match.Groups["PriorityCode"].Value);
			achFile.Destination = new FinancialInstitution(match.Groups["DestinationName"].Value, match.Groups["DestinationRTN"].Value);
			achFile.Origin = new FinancialInstitution(match.Groups["OriginName"].Value, match.Groups["OriginRTN"].Value);
			achFile.FileCreationDateTime = DateTime.ParseExact(match.Groups["FileDate"].Value, "yyMMddhhmm", null);
			achFile.FileIDModifier = match.Groups["FileModifier"].Value;
			achFile.ReferenceCode = match.Groups["ReferenceCode"].Value;
			return new ACHParserStateFile(achFile);
		}

		public ACHParserState MatchedACHFileFooter(Match match) {
			// TODO - Double-check parsed values against what we've loaded so far.
			int batchCount = int.Parse(match.Groups["BatchCount"].Value);
			int blockCount = int.Parse(match.Groups["BlockCount"].Value);
			int entryAddendaCount = int.Parse(match.Groups["EntryAddendaCount"].Value);
			decimal debitTotal = decimal.Parse(match.Groups["DebitTotal"].Value) / 100;
			decimal creditTotal = decimal.Parse(match.Groups["CreditTotal"].Value) / 100;
			long entryHash = long.Parse(match.Groups["EntryHash"].Value);
			try {
				if (achFile.Batches.Count != batchCount) {
					throw new ACHException("ACH File Footer Batch count did not match the parsed batch count.");
				}
				if (blockCount != achFile.BlockCount) {
					throw new ACHException("ACH File Footer Block count did not match the parsed block count.");
				}

				if (entryAddendaCount != achFile.EntryAddendaCount) {
					throw new ACHException("ACH File Footer Entry Addenda count did not match the parsed count.");
				}

				if (debitTotal != achFile.DebitTotal) {
					throw new ACHException("ACH File Footer DebitTotal did not match the parsed total.");
				}

				if (creditTotal != achFile.CreditTotal) {
					throw new ACHException("ACH File Footer CreditTotal did not match the parsed total.");
				}

				if (entryHash != achFile.EntryHash) {
					throw new ACHException("ACH File Footer Entry Hash did not match the parsed value.");
				}
			}
			catch (ACHException ex) {
				logger.Error("Error validating ACH File Footer, outputed reccord may be different from origional input", ex);
			}
			return this;
		}

		internal ACHParserState MatchedEndOfTransmission(Match match) {
			return null;
		}
	}
}
