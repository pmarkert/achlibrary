﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    internal class ACHParser {
        private ACHParserState CurrentState;

        internal ACHFile Parse(System.IO.TextReader reader) {
            ACHFile file = new ACHFile();
            CurrentState = new ACHParserStateNone(file);
            string line;
            while (CurrentState!=null && (line = reader.ReadLine()) != null) {
                CurrentState = CurrentState.ParseLine(line);
            }
            return file;
        }
    }
}
