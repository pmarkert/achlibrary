﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using log4net;

namespace Ephisys.ACH {
    internal class ACHParserStateFile : ACHParserState {
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private Regex reBatchHeaderRecord = new Regex(@"^5(?<ServiceClassCode>\d{3})(?<CompanyName>.{16})(?<CompanyDiscretionaryData>.{20})(?<CompanyIdentification>.{10})(?<StandardEntryClassCode>.{3})(?<CompanyEntryDescription>.{10})(?<CompanyDescriptiveDate>.{6})(?<EffectiveEntryDate>\d{6})(?<SettlementDate>\d{3})(?<OriginatorStatusCode>.)(?<OriginatingDFIIdentification>\d{8})(?<BatchNumber>\d{7})$", RegexOptions.Compiled);
		private Regex reBatchControlRecord = new Regex(@"^8(?<ServiceClassCode>\d{3})(?<EntryAddendaCount>\d{6})(?<EntryHash>\d{10})(?<TotalDebits>\d{12})(?<TotalCredits>\d{12})(?<CompanyIdentification>.{10})(?<MessageAuthenticationCode>.{19})\s{6}(?<OriginatingDFIIdentification>\d{8})(?<BatchNumber>\d{7})$", RegexOptions.Compiled);

		internal ACHParserStateFile(ACHFile file)
            : base(file) {
        }

        protected override LineMatchAction[] MatchActions {
			get {
				return new LineMatchAction[] {
                    new LineMatchAction() { Regex = reBatchHeaderRecord, Action = MatchedBatchHeaderRecord },
                    new LineMatchAction() { Regex = reBatchControlRecord, Action = MatchedBatchControlRecord }
                };
			}
        }


        protected override ACHParserState NextState {
            get {
                return new ACHParserStateNone(achFile);
            }
        }

		private ACHParserState MatchedBatchHeaderRecord(Match match) {
			int serviceClassCode = int.Parse(match.Groups["ServiceClassCode"].Value);
			string companyName = match.Groups["CompanyName"].Value.Trim();
			string companyDiscretionaryData = match.Groups["CompanyDiscretionaryData"].Value;
			string companyIdentification = match.Groups["CompanyIdentification"].Value;
			string standardEntryClassCode = match.Groups["StandardEntryClassCode"].Value;
			string companyEntryDescription = match.Groups["CompanyEntryDescription"].Value;
			string companyDescriptiveDate = match.Groups["CompanyDescriptiveDate"].Value;
			DateTime effectiveEntryDate = DateTime.ParseExact(match.Groups["EffectiveEntryDate"].Value, "yyMMdd", null, System.Globalization.DateTimeStyles.None);
			int settlementDate = int.Parse(match.Groups["SettlementDate"].Value);
			char originatorStatusCode = match.Groups["OriginatorStatusCode"].Value[0];
			int originatingDFIIdentification = int.Parse(match.Groups["OriginatingDFIIdentification"].Value);
			int batchNumber = int.Parse(match.Groups["BatchNumber"].Value);

			ACHBatch batch = achFile.AddBatch((ServiceClassCode)Enum.Parse(typeof(ServiceClassCode), serviceClassCode.ToString()), companyName, companyDiscretionaryData, companyIdentification, standardEntryClassCode, companyEntryDescription, companyDescriptiveDate, effectiveEntryDate, settlementDate, originatorStatusCode, originatingDFIIdentification, batchNumber);


			return new ACHParserStateBatch(this.achFile, batch);
		}

		private ACHParserState MatchedBatchControlRecord(Match match) {
			ServiceClassCode serviceClassCode = (ServiceClassCode)Enum.Parse(typeof(ServiceClassCode), int.Parse(match.Groups["ServiceClassCode"].Value).ToString());
			int entryAddendaCount = int.Parse(match.Groups["EntryAddendaCount"].Value);
			long entryHash = long.Parse(match.Groups["EntryHash"].Value);
			decimal totalDebits = decimal.Parse(match.Groups["TotalDebits"].Value)/100;
			decimal totalCredits = decimal.Parse(match.Groups["TotalCredits"].Value)/100;
			string companyIdentification = match.Groups["CompanyIdentification"].Value;
			string messageAuthenticationCode = match.Groups["MessageAuthenticationCode"].Value;
			int originatingDFIIdentification = int.Parse(match.Groups["OriginatingDFIIdentification"].Value);
			int batchNumber = int.Parse(match.Groups["BatchNumber"].Value);

			ACHBatchFooter footer = new ACHBatchFooter(achFile.Batches[achFile.Batches.Count - 1]);
			try {
				if (footer.ServiceClassCode != serviceClassCode) {
					throw new ACHException("ACH Batch Footer ServiceClassCode did not match the parsed value");
				}
				if (footer.EntryAddendaCount != entryAddendaCount) {
					throw new ACHException("ACH Batch Footer EntryAddendaCount did not match the Entry/Addenda count");
				}
				if (footer.EntryHash != entryHash) {
					throw new ACHException("ACH Batch Footer entryHash did not match the parsed value");
				}
				if (footer.TotalDebitEntryDollarAmount != totalDebits) {
					throw new ACHException("ACH Batch Footer Total Debits did not match parsed value");
				}
				if (footer.TotalCreditEntryDollarAmount != totalCredits) {
					throw new ACHException("ACH Batch Footer total credits did not match parsed value");
				}
				if (footer.CompanyIdentification != companyIdentification) {
					throw new ACHException("ACH Batch Footer Company Identification did not match parsed value");
				}
				if (footer.MessageAuthenticationCode != messageAuthenticationCode.Trim()) {
					throw new ACHException("ACH Batch Footer message authentication code did not match parsed value");
				}
				if (footer.OriginatingDFIID != originatingDFIIdentification) {
					throw new ACHException("ACH Batch Footer originating DFI Identification did not match parsed value");
				}
			}
			catch (ACHException ex) {
				logger.Error("Error Validating ACHBatchFooter, outputed record may be different from origional input", ex);
			}
			footer.BatchNumber = batchNumber;

			return this;
		}
	}
}
