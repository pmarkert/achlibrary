﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ephisys.ACH {
	internal class ACHParserStateBatch : ACHParserState {
		ACHBatch batch;

		internal ACHParserStateBatch(ACHFile achFile, ACHBatch batch)
			: base(achFile) {
			this.batch = batch;
		}

		private Regex reEntry = new Regex(@"^6(?<TransactionCode>\d{2})(?<ReceivingRTN>\d{9})(?<ReceivingAccountNumber>.{17})(?<Amount>\d{10})(?<IdentificationNumber>.{15})(?<ReceivingCompanyName>.{22})(?<DiscretionaryData>.{2})(?<AddendaRecordIndicator>\d)(?<TraceNumber>\d{15})$");
		private Regex reAddendum = new Regex(@"^705(?<PaymentInformation>.{80})(?<SequenceNumber>\d{4})(?<EntryDetailSequenceNumber>\d{7})$");
		private Regex reOtherAddendum = new Regex(@"^7(?<Data>.{93})$");


		internal ACHParserStateBatch(ACHFile file)
			: base(file) {
		}

		protected override LineMatchAction[] MatchActions {
			get {
				return new LineMatchAction[] { 
                    new LineMatchAction() { Regex = reEntry, Action = MatchedEntry },
                    new LineMatchAction() { Regex = reAddendum, Action = MatchedAddendum },
					new LineMatchAction() { Regex = reOtherAddendum, Action = MathedOtherAddendum}
                };
			}
		}

		protected override ACHParserState NextState {
			get {
				return new ACHParserStateFile(achFile);
			}
		}

		private ACHParserState MatchedEntry(Match match) {
			ACHEntry entry = new ACHEntry(batch);
			entry.TransactionCode = TransactionCodeFactory.GetById(int.Parse(match.Groups["TransactionCode"].Value));
			entry.ReceivingDFIID = match.Groups["ReceivingRTN"].Value;
			entry.DfiAccountNumber = match.Groups["ReceivingAccountNumber"].Value;
			entry.Amount = decimal.Parse(match.Groups["Amount"].Value) / 100;
			entry.ID = match.Groups["IdentificationNumber"].Value;
			entry.ReceivingCompanyName = match.Groups["ReceivingCompanyName"].Value;
			entry.DiscretionaryData = match.Groups["DiscretionaryData"].Value;
			entry.AddendaRecordIndicator = int.Parse(match.Groups["AddendaRecordIndicator"].Value);
			entry.TraceNumber = match.Groups["TraceNumber"].Value;
			batch.Records.Add(entry);
			return this;
		}

		private ACHParserState MatchedAddendum(Match match) {
			ACHAddendum addendum = batch.AddAddendum();
			addendum.PaymentRelatedInformation = match.Groups["PaymentInformation"].Value;
			addendum.EntryDetailSequenceNumber = int.Parse(match.Groups["EntryDetailSequenceNumber"].Value);
			addendum.SequenceNumber = int.Parse(match.Groups["SequenceNumber"].Value);
			//batch.Records.Add(addendum);
			return this;
		}
		private ACHParserState MathedOtherAddendum(Match match) {
			ACHAddendum addendum = batch.AddAddendum();
			addendum.OtherACHString = match.Groups["Data"].Value;
			//batch.Records.Add(addendum);
			return this;
		}
	}
}
