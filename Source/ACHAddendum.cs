﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    public class ACHAddendum : ACHEntryAddendum{
        internal const int ADDENDUM_TYPE_CODE = 5;

        internal ACHAddendum(ACHBatch owningBatch) {
            SequenceNumber = owningBatch.OwningACHFile.GetNextSequenceNumber();
        }

        public ACHAddendum(ACHBatch owninBatch, ACHEntry entry) {
            SequenceNumber = owninBatch.OwningACHFile.GetNextSequenceNumber();
            EntryDetailSequenceNumber = entry.SequenceNumber;
        }

		public string OtherACHString {
			get;
			set;
		}
		

        private char RecordType {
            get { return '7'; }
        }

        public string PaymentRelatedInformation {
            get;
            internal set;
        }

        public int SequenceNumber {
            get;
            internal set;
        }

        public int EntryDetailSequenceNumber {
            get;
            internal set;
        }

        public override string ToString() {
            PaddedStringBuilder sBuilder = new PaddedStringBuilder(ACHFile.RECORD_LENGTH);
            sBuilder.Append(RecordType);
			if (!string.IsNullOrEmpty(OtherACHString)) {
				sBuilder.Append(OtherACHString, 93);
			}
			else {
				sBuilder.Append(ADDENDUM_TYPE_CODE, 2);
				sBuilder.Append(PaymentRelatedInformation, 80);
				sBuilder.Append(SequenceNumber, 4);
				sBuilder.Append(EntryDetailSequenceNumber, 7);
			}
			return sBuilder.ToString();
        }
    }
}
