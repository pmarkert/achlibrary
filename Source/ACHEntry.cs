using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    public class ACHEntry : ACHEntryAddendum {
        #region private members
        private string dfiAccountNumber;
        private decimal amount;
        private string receivingCompanyName;
        private string discretionaryData = String.Empty;
        private TransactionCode transactionCode;
        private int addendaRecordIndicator = 0;
        #endregion

		private ACHBatch owningBatch;

		internal ACHEntry(ACHBatch owningBatch) {
            this.owningBatch = owningBatch;
            ReceivingDFIID = owningBatch.OwningACHFile.Destination.RoutingNumber;
            TraceNumber = owningBatch.OwningACHFile.Origin.RoutingNumber.Substring(0, 8);
            owningBatch.OwningACHFile.GetNextSequenceNumber();
        }

		public TransactionCode TransactionCode {
            get { return transactionCode; }
            set { transactionCode = value; }
        }

        public string ReceivingDFIID {
            get;
            internal set;
        }

        public string DfiAccountNumber {
            get { return dfiAccountNumber; }
            set { dfiAccountNumber = value; }
        }

        public decimal Amount {
            get { return amount; }
            set { amount = value; }
        }

        public string ID {
            get;
            internal set;
        }

        public string ReceivingCompanyName {
            get { return receivingCompanyName; }
            set { receivingCompanyName = value; }
        }

        public string DiscretionaryData {
            get { return discretionaryData; }
            set { discretionaryData = value; }
        }

        public int AddendaRecordIndicator {
            get { return addendaRecordIndicator; }
            set { addendaRecordIndicator = value; }
        }

        public string TraceNumber {
            get;
            internal set;
        }

        public int SequenceNumber {
            get;
            internal set;
        }

        private char RecordType {
            get { return '6'; }
        }

        public override string ToString() {
			PaddedStringBuilder sBuilder = new PaddedStringBuilder(ACHFile.RECORD_LENGTH);
            sBuilder.Append(RecordType);
            sBuilder.Append(TransactionCode.TransactionCodeId, 2);
            sBuilder.Append(ReceivingDFIID, 9);
            sBuilder.Append(DfiAccountNumber, 17);
            sBuilder.Append(Amount, 10);
            sBuilder.Append(ID, 15);
            sBuilder.Append(ReceivingCompanyName, 22);
            sBuilder.Append(DiscretionaryData, 2);
            sBuilder.Append(AddendaRecordIndicator, 1);
			sBuilder.Append(TraceNumber, 15);
            //sBuilder.Append(TraceNumber, 8);
            //sBuilder.Append(SequenceNumber, 7);
            return sBuilder.ToString();
        }
    }
}