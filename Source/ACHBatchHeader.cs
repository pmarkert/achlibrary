using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Ephisys.ACH {

    internal class ACHBatchHeader {
		private ACHBatch batch;
		
		internal ACHBatchHeader(ACHBatch batch) {
			this.batch = batch;
		}

		public override string ToString() {
			PaddedStringBuilder sBuilder = new PaddedStringBuilder(ACHFile.RECORD_LENGTH);
            sBuilder.Append(RecordType);
            sBuilder.Append((int)batch.ServiceClassCode,3);
            sBuilder.Append(batch.CompanyName, 16);
            sBuilder.Append(batch.CompanyDiscretionaryData, 20);
            sBuilder.Append(batch.CompanyId, 10);
            sBuilder.Append(batch.StandardEntryClassCode, 3);
            sBuilder.Append(batch.CompanyEntryDescription, 10);
            sBuilder.Append(batch.CompanyDescriptiveDate, 6);
            sBuilder.Append(batch.EffectiveEntryDate, 6);
            sBuilder.Append(batch.SettlementDate, 3);
			sBuilder.Append(batch.OrigionatorStatusCode, 1);
            sBuilder.Append(batch.OriginatingDFIID, 8);
            sBuilder.Append(batch.BatchNumber, 7);

            return sBuilder.ToString();
        }




		
        private char RecordType {
            get {
                return '5';
            }
        }
    }
}
