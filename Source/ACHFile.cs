using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Linq;
using log4net;
using System.Configuration;

namespace Ephisys.ACH {
	public class ACHFile {
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		public const int RECORD_LENGTH = 94;
		public const int BLOCKING_FACTOR = 10;
		public const string DEFAULT_FILEIDMODIFIER = "1";
		public const string START_OF_TRANSMISSION_FORMAT = "$IDENT00{0}N";
		public const string END_OF_TRANSMISSION_FORMAT = "$ENDFILE{0}N";
		protected internal List<ACHBatch> Batches;

		private int nextSequenceNumber;

		internal int GetNextSequenceNumber() {
			return Interlocked.Increment(ref nextSequenceNumber);
		}

		private string processingInstitutionFIID;
		public string ProcessingInstitutionFIID {
			get {
				if (String.IsNullOrEmpty(processingInstitutionFIID)) {
					return Origin.RoutingNumber;
				}
				else {
					return processingInstitutionFIID;
				}
			}
			set {
				FinancialInstitution.ValidateRoutingNumber(value);
				processingInstitutionFIID = value;
			}
		}

		public FinancialInstitution Destination {
			get;
			internal set;
		}

		public FinancialInstitution Origin {
			get;
			internal set;
		}


		internal int FormatCode {
			get { return 1; }
		}

		public int PriorityCode {
			get;
			internal set;
		}

		public DateTime FileCreationDateTime {
			get;
			internal set;
		}

		protected internal ACHFile() {
			InitializeDefaultValues();
			FileCreationDateTime = DateTime.Now;
		}

		private void InitializeDefaultValues() {
			PriorityCode = 1;
			FileIDModifier = DEFAULT_FILEIDMODIFIER;
		}

		public ACHFile(FinancialInstitution origin, FinancialInstitution destination) {
			this.Origin = origin;
			this.Destination = destination;
			Batches = new List<ACHBatch>();
			InitializeDefaultValues();
		}

		private string fileIDModifier = DEFAULT_FILEIDMODIFIER;
		public string FileIDModifier {
			get { return fileIDModifier; }
			set {
				if (!Regex.IsMatch(fileIDModifier, "^[A-Z0-9]$")) {
					throw new ACHException("FileIdModifier must be a single upper-case character or digit.");
				}
				fileIDModifier = value;
			}
		}

		private string referenceCode;
		public string ReferenceCode {
			get { return referenceCode; }
			set { referenceCode = value; }
		}

		public ACHBatch AddBatch(ServiceClassCode serviceClassCode, string companyName, string companyDiscretionaryData, string companyIdentification, string standardEntryClassCode, string companyEntryDescription,
			string companyDescriptiveDate, DateTime effectiveEntryDate, int settlementDate, char origionatorStatusCode, int originatingDFIIdentification, int batchNumber) {

			ACHBatch b = new ACHBatch(this, serviceClassCode, companyName, companyDiscretionaryData, companyIdentification, standardEntryClassCode, companyEntryDescription, companyDescriptiveDate, effectiveEntryDate, settlementDate, origionatorStatusCode, originatingDFIIdentification, batchNumber);
			if (Batches == null) {
				Batches = new List<ACHBatch>();
			}
			Batches.Add(b);
			return b;
		}


		public ACHBatch AddBatch(string companyName, ServiceClassCode serviceClassCode) {
			ACHBatch b = new ACHBatch(this, companyName, serviceClassCode);
			if (Batches == null) {
				Batches = new List<ACHBatch>();
			}
			Batches.Add(b);
			return b;
		}

		private int lineCount {
			get {
				int lines = 2;
				lines += (Batches.Count * 2);
				foreach (ACHBatch b in Batches) {
					lines += b.RecordCount();
				}
				return lines;
			}
		}

		public void WriteACHFile(TextWriter tw) {
			tw.WriteLine(String.Format(START_OF_TRANSMISSION_FORMAT, ProcessingInstitutionFIID));
			tw.Write(this.RenderACHFileContent());
			tw.WriteLine(String.Format(END_OF_TRANSMISSION_FORMAT, ProcessingInstitutionFIID));
		}

		private string RenderACHFileContent() {
			StringBuilder sBuilder = new StringBuilder(ACHFile.RECORD_LENGTH);

			sBuilder.AppendLine(CreateHeader());

			foreach (ACHBatch batch in Batches) {
				sBuilder.AppendLine(batch.ToString());
			}
			sBuilder.AppendLine(CreateFooter());
			if (lineCount != 10) {
				for (int i = 0; i < (10 - (lineCount % 10)); ++i) {
					sBuilder.AppendLine(new string('9', ACHFile.RECORD_LENGTH));
				}
			}

			return sBuilder.ToString();
		}

		internal int EntryAddendaCount {
			get {
				int count = 0;
				foreach (ACHBatch b in Batches) {
					count += b.RecordCount();
				}
				return count;
			}
		}

		internal long EntryHash {
			get {
				long hash = 0;
				foreach (ACHBatch bat in Batches) {
					hash += bat.Hash;
					if (hash > 10000000000) {
						hash = hash % 10000000000;
					}
				}
				return hash;
			}
		}

		internal decimal DebitTotal {
			get { return GetAmount(TransactionCodeType.Debit); }
		}

		internal decimal CreditTotal {
			get { return GetAmount(TransactionCodeType.Credit); }
		}

		private decimal GetAmount(TransactionCodeType transactionCodeType) {
			decimal total = 0;
			foreach (ACHBatch bat in Batches) {
				total += Utils.GetAmount(bat.Records, transactionCodeType);
			}
			return total;
		}

		private string CreateHeader() {
			return new ACHFileHeader(this).ToString();
		}

		private string CreateFooter() {
			return new ACHFileFooter(this).ToString();
		}

		internal int BlockCount {
			get { return (lineCount / 10) + 1; }
		}

		public void ClearBatches() {
			Batches.Clear();
		}

		public static ACHFile ParseACHFile(TextReader reader) {
			return new ACHParser().Parse(reader);
		}

		public void CompactBatch(string CompanyNameToCompact) {
			IEnumerable<ACHBatch> batchesToCompact = (from ACHBatch b in Batches where b.CompanyName.Trim() == CompanyNameToCompact select b);
			if (batchesToCompact.Count() == 0) {
				logger.Warn(string.Format("No batch matches company name {0}, nothing will be compacted", CompanyNameToCompact));
			}
			foreach (ACHBatch batch in batchesToCompact) {
				if (batch.Records.Count == 0) {
					logger.Warn("Batch contains no entries");
				}

				ACHEntry totalEntry = null;
				for (int i = 0; i < batch.Records.Count; ++i) {
					if ((batch.Records[i] as ACHEntry) == null) {
						logger.Warn(string.Format("Record {0} is not an ACHEntry, it will be removed when batch is compacted", i));
					}
					else {
						ACHEntry entry = batch.Records[i] as ACHEntry;
						if (entry.TransactionCode != TransactionCodeFactory.GetById(22)) {
							logger.Warn(string.Format("Entry contains another transaction code other than 22 ({0}), this may cause problems with total batch amount being off", entry.TransactionCode.TransactionCodeId + " - " + entry.TransactionCode.Description));
						}
						else {
							if (totalEntry == null) {
								totalEntry = entry;
							}
							else {
								totalEntry.Amount += entry.Amount;
							}
						}
					}
				}
				batch.ClearRecords();
				ACHEntry newEntry = batch.AddEntry();
				newEntry.AddendaRecordIndicator = totalEntry.AddendaRecordIndicator;
				newEntry.Amount = totalEntry.Amount;
				newEntry.DfiAccountNumber = totalEntry.DfiAccountNumber;
				newEntry.DiscretionaryData = totalEntry.DiscretionaryData;
				newEntry.ID = totalEntry.ID;
				newEntry.ReceivingCompanyName = ConfigurationManager.AppSettings["ReceivingCompanyName"];
				newEntry.ReceivingDFIID = totalEntry.ReceivingDFIID;
				newEntry.SequenceNumber = totalEntry.SequenceNumber;
				newEntry.TraceNumber = totalEntry.TraceNumber;
				newEntry.TransactionCode = totalEntry.TransactionCode;
			}
		}
	}
}
