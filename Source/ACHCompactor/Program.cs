﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ephisys.ACH;
using System.Configuration;
using log4net;

namespace ACHCompactor {
	class Program {
		private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


		static void Main(string[] args) {
			log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo("log4net.config"));
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);


			if (args.Length != 2) {
				Console.WriteLine("Usage: ACHCompactor InputFileName OutputFileName");
			}
			else {
				using (StreamReader sr = new StreamReader(args[0])) {
					logger.Debug("Reading in " + args[0]);
					ACHFile achFile = ACHFile.ParseACHFile(sr);
					string CompanyName = ConfigurationManager.AppSettings["CompanyNameToCompact"].Trim();
					logger.Debug("Compacting Batch " + CompanyName);
                    achFile.CompactBatch(CompanyName);
					using (StreamWriter owriter = new StreamWriter(args[1])) {
						logger.Debug("Writing out new ach file: " + args[1]);
						achFile.WriteACHFile(owriter);
					}
				}
			}
			logger.Info("Done");
		}

		static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
			logger.Fatal("Unhandled exception caught by AppDomain handler.  Application Terminating=" + e.IsTerminating, e.ExceptionObject as Exception);
		}
	}
}
