using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    internal class ACHBatchFooter {

        private ACHBatch batch;

        public ACHBatchFooter(ACHBatch b) {
            batch = b;
        }

        private int batchNumber;

        private char RecordTypeCode {
            get { return '8'; }
        }

        public int EntryAddendaCount {
            get { return batch.RecordCount(); }
        }

        public decimal TotalCreditEntryDollarAmount {
            get {
                return Utils.GetAmount(batch.Records, TransactionCodeType.Credit);
            }
        }

        public decimal TotalDebitEntryDollarAmount {
            get {
                return Utils.GetAmount(batch.Records, TransactionCodeType.Debit);
            }
        }

        public string MessageAuthenticationCode {
            get { return String.Empty; }
        }

        public int BatchNumber {
            get { return batchNumber; }
            set { batchNumber = value; }
        }

        internal long EntryHash {
            get {
                long hash = 0;
				foreach (ACHEntryAddendum rec in batch.Records) {
					if (rec is ACHEntry) {
						hash += long.Parse(((ACHEntry)rec).ReceivingDFIID.Substring(0, 8));
						if (hash > 10000000000) {
							hash = hash % 10000000000;
						}
					}
				}
                return hash;
            }
        }

		public ServiceClassCode ServiceClassCode {
			get {
				return batch.ServiceClassCode;
			}
		}

		public string CompanyIdentification {
			get {
				return batch.CompanyId;
			}
		}

		public int OriginatingDFIID {
			get { return batch.OriginatingDFIID; }
		}

        public override string ToString() {
			PaddedStringBuilder sBuilder = new PaddedStringBuilder(ACHFile.RECORD_LENGTH);
            sBuilder.Append(RecordTypeCode);
            sBuilder.Append((int)ServiceClassCode, 3);
            sBuilder.Append(EntryAddendaCount, 6);
            sBuilder.Append(EntryHash, 10);
            sBuilder.Append(TotalDebitEntryDollarAmount, 12);
            sBuilder.Append(TotalCreditEntryDollarAmount, 12);
            sBuilder.Append(CompanyIdentification, 10);
            sBuilder.Append(MessageAuthenticationCode, 19);
            sBuilder.Append(' ', 6);
            sBuilder.Append(OriginatingDFIID, 8);
            sBuilder.Append(BatchNumber, 7);
            return sBuilder.ToString();
        }
    }
}
