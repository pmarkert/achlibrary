using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    public class ACHFileFooter {
        private ACHFile achFile;

        public ACHFileFooter(ACHFile file) {
            achFile = file;
        }
        public override string ToString() {
			PaddedStringBuilder sBuilder = new PaddedStringBuilder(ACHFile.RECORD_LENGTH);
            sBuilder.Append(RecordType);
            sBuilder.Append(achFile.Batches.Count, 6);
            sBuilder.Append(achFile.BlockCount, 6);
            sBuilder.Append(achFile.EntryAddendaCount, 8);
            sBuilder.Append(achFile.EntryHash, 10);
            sBuilder.Append(achFile.DebitTotal, 12);
            sBuilder.Append(achFile.CreditTotal, 12);
            sBuilder.Append(String.Empty, 39);
            return sBuilder.ToString();
        }

        private char RecordType {
            get { return '9'; }
        }
	}
}