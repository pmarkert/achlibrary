using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Ephisys.ACH {
	public static class TransactionCodeFactory {
		private static List<TransactionCode> KnownTransactionCodes;

		static TransactionCodeFactory() {
			KnownTransactionCodes = new List<TransactionCode>();
			KnownTransactionCodes.Add(new TransactionCode(20, TransactionCodeType.Credit, "Demand Credit Reserved"));
			KnownTransactionCodes.Add(new TransactionCode(21, TransactionCodeType.Credit, "Demand Credit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(22, TransactionCodeType.Credit, "Demand Credit Automated Deposit"));
			KnownTransactionCodes.Add(new TransactionCode(23, TransactionCodeType.Credit, "Demand Credit Prenotifacion Auth"));
			KnownTransactionCodes.Add(new TransactionCode(24, TransactionCodeType.Credit, "Demand Credit Zero Dollar"));
			KnownTransactionCodes.Add(new TransactionCode(25, TransactionCodeType.Debit, "Demand Debit Reserved"));
			KnownTransactionCodes.Add(new TransactionCode(26, TransactionCodeType.Debit, "Demand Debit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(27, TransactionCodeType.Debit, "Demand Debit Automated Payment"));
			KnownTransactionCodes.Add(new TransactionCode(28, TransactionCodeType.Debit, "Demand Debit Prenotification Auth"));
			KnownTransactionCodes.Add(new TransactionCode(29, TransactionCodeType.Debit, "Demand Debit Reserved"));
			KnownTransactionCodes.Add(new TransactionCode(30, TransactionCodeType.Credit, "Savings Account Credit Reserved"));
			KnownTransactionCodes.Add(new TransactionCode(31, TransactionCodeType.Credit, "Savings Account Credit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(32, TransactionCodeType.Credit, "Savings Account Credit Automated Deposit"));
			KnownTransactionCodes.Add(new TransactionCode(33, TransactionCodeType.Credit, "Savings Account Credit Perenotification Auth"));
			KnownTransactionCodes.Add(new TransactionCode(34, TransactionCodeType.Credit, "Savings Account Credit Zero Dollar"));
			KnownTransactionCodes.Add(new TransactionCode(35, TransactionCodeType.Debit, "Savings Account Debit Reserved"));
			KnownTransactionCodes.Add(new TransactionCode(36, TransactionCodeType.Debit, "Savings Account Debit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(37, TransactionCodeType.Debit, "Savings Account Debit Automated Payment"));
			KnownTransactionCodes.Add(new TransactionCode(38, TransactionCodeType.Debit, "Savings Account Debit Prenotification Auth"));
			KnownTransactionCodes.Add(new TransactionCode(39, TransactionCodeType.Debit, "Savings Account Debit Zero Dollar"));
			KnownTransactionCodes.Add(new TransactionCode(41, TransactionCodeType.Credit, "FIGL Credit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(42, TransactionCodeType.Credit, "FIGL Credit Automated Deposit"));
			KnownTransactionCodes.Add(new TransactionCode(43, TransactionCodeType.Credit, "FIGL Credit Prenotification Auth"));
			KnownTransactionCodes.Add(new TransactionCode(44, TransactionCodeType.Credit, "FIGL Credit Zero Dollar"));
			KnownTransactionCodes.Add(new TransactionCode(46, TransactionCodeType.Debit, "FIGL Debit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(47, TransactionCodeType.Debit, "FIGL Debit Automated Payment"));
			KnownTransactionCodes.Add(new TransactionCode(48, TransactionCodeType.Debit, "FIGL Debit Prenotification Auth"));
			KnownTransactionCodes.Add(new TransactionCode(49, TransactionCodeType.Debit, "FIGL Debit Zero Dollar"));
			KnownTransactionCodes.Add(new TransactionCode(51, TransactionCodeType.Credit, "Loan Account Credit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(52, TransactionCodeType.Credit, "Loan Account Credit Automated Deposit"));
			KnownTransactionCodes.Add(new TransactionCode(53, TransactionCodeType.Credit, "Loan Account Credit Prenotificaion Auth"));
			KnownTransactionCodes.Add(new TransactionCode(54, TransactionCodeType.Credit, "Loan Account Credit Zero Dollar"));
			KnownTransactionCodes.Add(new TransactionCode(55, TransactionCodeType.Debit, "Loan Account Debit Automated Debit"));
			KnownTransactionCodes.Add(new TransactionCode(56, TransactionCodeType.Debit, "Loan Account Debit Automated Return"));
			KnownTransactionCodes.Add(new TransactionCode(81, TransactionCodeType.Credit, "Credit for ACH debits originated"));
			KnownTransactionCodes.Add(new TransactionCode(82, TransactionCodeType.Debit, "Debit for ACH debits originated"));
			KnownTransactionCodes.Add(new TransactionCode(83, TransactionCodeType.Credit, "Credit for ACH debits recieved"));
			KnownTransactionCodes.Add(new TransactionCode(84, TransactionCodeType.Debit, "Debit for ACH debits recieved"));
			KnownTransactionCodes.Add(new TransactionCode(85, TransactionCodeType.Credit, "Credit for ACH credits in rejected batches"));
			KnownTransactionCodes.Add(new TransactionCode(86, TransactionCodeType.Debit, "Debit for ACH credits in rejected batches"));
			KnownTransactionCodes.Add(new TransactionCode(87, TransactionCodeType.Credit, "Summary credit for respondent ACH activity"));
			KnownTransactionCodes.Add(new TransactionCode(88, TransactionCodeType.Credit, "Summary debit for respondent ACH activity"));
		}

		public static TransactionCode GetById(int TransactionCodeId) {
			TransactionCode transactionCode = (from TransactionCode tc in KnownTransactionCodes where tc.TransactionCodeId == TransactionCodeId select tc).FirstOrDefault();
			if (transactionCode == null) {
				transactionCode = new TransactionCode(TransactionCodeId, TransactionCodeType.Unknown, "Unknown Code (" + TransactionCodeId + ")");
				//logger.warn
			}
			return transactionCode;
		}
	}
	public enum TransactionCodeType {
		Credit,
		Debit,
		Unknown
	}


	public class TransactionCode {
		public int TransactionCodeId;
		public TransactionCodeType TransactionCodeType;
		public string Description;

		public TransactionCode(int transactionCodeId, TransactionCodeType transactionCodeType, string description) {
			TransactionCodeId = transactionCodeId;
			TransactionCodeType = transactionCodeType;
			Description = description;
		}
	}
}
