using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
	public enum ServiceClassCode {
		MixedDebitsAndCredits = 200,
		CreditsOnly = 220,
		DebitsOnly = 225,
		AutomatedAccountingAdvices = 280
	}
}
