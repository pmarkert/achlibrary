using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
	public class ACHBatch {
		private void InitializeDefaultValues() {
			if (string.IsNullOrEmpty(StandardEntryClassCode)) {
				StandardEntryClassCode = "CCD";
			}
			if (string.IsNullOrEmpty(CompanyEntryDescription)) {
				CompanyEntryDescription = CompanyName;
			}
			if (string.IsNullOrEmpty(CompanyId)) {
				CompanyId = " " + OwningACHFile.Origin.RoutingNumber;
			}
			if (OriginatingDFIID == 0) {
				string rtn = OwningACHFile.Origin.RoutingNumber;
				if (rtn.Length > 0) {
					rtn = rtn.Substring(0, rtn.Length - 1);
				}
				OriginatingDFIID = int.Parse(rtn);
			}
			//if (CompanyDescriptiveDate == DateTime.MinValue) { //i think that works
			//    CompanyDescriptiveDate = DateTime.Now;
			//}
			if (EffectiveEntryDate == DateTime.MinValue) {
				EffectiveEntryDate = DateTime.Now;
			}
			if (SettlementDate == 0) {
				SettlementDate = DateTime.Now.Subtract(new DateTime(DateTime.Today.Year, 1, 1)).Days + 1;
			}
			if (string.IsNullOrEmpty(OrigionatorStatusCode)) {
				OrigionatorStatusCode = "1";
			}

		}

		public string StandardEntryClassCode {
			get;
			internal set;
		}

		internal ACHFile OwningACHFile {
			get;
			private set;
		}

		public string CompanyName {
			get;
			internal set;
		}


		public ServiceClassCode ServiceClassCode {
			get;
			internal set;
		}

		public string CompanyEntryDescription {
			get;
			internal set;
		}

		public string CompanyId {
			get;
			internal set;
		}

		public int OriginatingDFIID {
			get;
			internal set;
		}

		public string CompanyDescriptiveDate {
			get;
			internal set;
		}

		public DateTime EffectiveEntryDate {
			get;
			internal set;
		}

		public int SettlementDate {
			get;
			internal set;
		}

		internal long Hash {
			get {
				ACHBatchFooter footer = new ACHBatchFooter(this);
				return footer.EntryHash;
			}
		}

		public string CompanyDiscretionaryData {
			get;
			set;
		}
		
		public string OrigionatorStatusCode {
			get;
			internal set;
		}
        public int BatchNumber {
			get;
			set;
		}

		internal ACHBatch(ACHFile owningACHFile, string companyName)
			: this(owningACHFile, companyName, ServiceClassCode.MixedDebitsAndCredits) {
		}

		internal ACHBatch(ACHFile owningACHFile, string companyName, ServiceClassCode serviceClassCode) {
			if (String.IsNullOrEmpty(companyName)) {
				throw new ArgumentException("CompanyName cannot be null or empty.", companyName);
			}
			this.OwningACHFile = owningACHFile;
			this.ServiceClassCode = serviceClassCode;
			this.CompanyName = companyName;
			Records = new List<ACHEntryAddendum>();
			//BatchNumber = owningACHFile.Batches.Count + 1;
		}

		public ACHBatch(ACHFile aCHFile, ACH.ServiceClassCode serviceClassCode, string companyName, string companyDiscretionaryData, string companyIdentification, string standardEntryClassCode, 
			string companyEntryDescription, string companyDescriptiveDate, DateTime effectiveEntryDate, int settlementDate, char origionatorStatusCode, int originatingDFIIdentification, int batchNumber)
			: this(aCHFile,companyName,serviceClassCode) 	{
			this.CompanyDiscretionaryData = companyDiscretionaryData;
			this.CompanyId = companyIdentification;
			this.StandardEntryClassCode = standardEntryClassCode;
			this.CompanyEntryDescription = companyEntryDescription;
			this.CompanyDescriptiveDate = companyDescriptiveDate;
			this.EffectiveEntryDate = effectiveEntryDate;
			this.SettlementDate = settlementDate;
			this.OrigionatorStatusCode = origionatorStatusCode.ToString();
			this.OriginatingDFIID = originatingDFIIdentification;
			this.BatchNumber = batchNumber;

		}


		public List<ACHEntryAddendum> Records {
			get;
			internal set;
		}

		public override string ToString() {
			StringBuilder sBuilder = new StringBuilder(ACHFile.RECORD_LENGTH);

			sBuilder.AppendLine(CreateHeader());
			foreach (ACHEntryAddendum record in Records) {
				sBuilder.AppendLine(record.ToString());
			}
			sBuilder.Append(CreateFooter());
			return sBuilder.ToString();
		}

		private string CreateHeader() {
			ACHBatchHeader header = new ACHBatchHeader(this);
			return header.ToString();
		}

		private string CreateFooter() {
			ACHBatchFooter footer = new ACHBatchFooter(this);
			footer.BatchNumber = BatchNumber;
			return footer.ToString();
		}

		public ACHEntry AddEntry() {
			ACHEntry r = new ACHEntry(this);
			Records.Add(r);
			return r;
		}

		public ACHAddendum AddAddendum() {
			ACHAddendum a = new ACHAddendum(this);
			Records.Add(a);
			return a;
		}

		public ACHAddendum AddAddendum(ACHEntry entry) {
			ACHAddendum a = new ACHAddendum(this, entry);
			Records.Add(a);
			return a;
		}

		public void ClearRecords() {
			Records.Clear();
		}

		internal int RecordCount() {
			return Records.Count;
		}
	}
}
