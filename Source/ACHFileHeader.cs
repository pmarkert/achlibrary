using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ephisys.ACH {
    internal class ACHFileHeader {
		private ACHFile owningACHFile;

		internal ACHFileHeader(ACHFile owningACHFile) {
			this.owningACHFile = owningACHFile;
		}

        public override string ToString() {
			PaddedStringBuilder sBuilder = new PaddedStringBuilder(ACHFile.RECORD_LENGTH);
            sBuilder.Append(RecordType);
            sBuilder.Append(owningACHFile.PriorityCode, 2);
            sBuilder.Append(" " + owningACHFile.Destination.RoutingNumber, 10);
            sBuilder.Append(" " + owningACHFile.Origin.RoutingNumber, 10);
            sBuilder.Append(owningACHFile.FileCreationDateTime, 10);
            sBuilder.Append(owningACHFile.FileIDModifier, 1);
            sBuilder.Append(ACHFile.RECORD_LENGTH, 3);
            sBuilder.Append(ACHFile.BLOCKING_FACTOR, 2);
            sBuilder.Append(owningACHFile.FormatCode, 1);
            sBuilder.Append(owningACHFile.Destination.Name, 23);
            sBuilder.Append(owningACHFile.Origin.Name, 23);
			sBuilder.Append(owningACHFile.ReferenceCode, 8);
            
            return sBuilder.ToString();
        }

        internal char RecordType {
            get { return '1'; }
        }
    }
}
