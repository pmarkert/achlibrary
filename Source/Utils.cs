using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    internal static class Utils {
        internal static decimal GetAmount(System.Collections.Generic.List<ACHEntryAddendum> Records, TransactionCodeType transactionCodeType) {
            decimal amount = 0;
            foreach (ACHEntryAddendum rec in Records) {
                if (rec is ACHEntry) {
                    ACHEntry entry = (ACHEntry)rec;
                    if (entry.TransactionCode.TransactionCodeType == transactionCodeType) {
                        amount += entry.Amount;
                    }
                }
            }
            return amount;
        }
    }
}
