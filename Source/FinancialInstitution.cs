using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ephisys.ACH {
	public class FinancialInstitution {
        public string Name {
            get;
            protected set;
        }

        public string RoutingNumber {
            get;
            protected set;
        }

        internal static void ValidateRoutingNumber(string routingNumber) {
            if (!Regex.IsMatch(routingNumber, @"^\d{9}$")) {
                throw new ACHException("Routing number must be a 9-digit number.");
            }
        }
		
        public FinancialInstitution(string name, string routingNumber) {
            ValidateRoutingNumber(routingNumber);
			this.Name = name;
			this.RoutingNumber = routingNumber;
		}
	}
}
