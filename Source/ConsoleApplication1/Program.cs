﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ephisys.ACH;

namespace ConsoleApplication1 {
    class Program {
        static void Main(string[] args) {
            using (StreamReader sr = new StreamReader(args[0])) {
				ACHFile achFile = ACHFile.ParseACHFile(sr);
				//achFile.WriteACHFile(Console.Out);
				using (StreamWriter owriter = new StreamWriter("out.txt")) {
					achFile.WriteACHFile(owriter);
				}
				//Console.ReadKey();
            }
        }
    }
}
