using System;
using System.Collections.Generic;
using System.Text;

namespace Ephisys.ACH {
    internal class PaddedStringBuilder {
        private StringBuilder sBuilder;

        internal PaddedStringBuilder() {
            sBuilder = new StringBuilder();
        }

        internal PaddedStringBuilder(int capacity) {
            sBuilder = new StringBuilder(capacity);
        }

        internal void Append(object value, int length) {
            sBuilder.Append(PadAndFormat(value, length));
        }

        internal void Append(object value) {
            sBuilder.Append(value);
        }

        public override string ToString() {
            return sBuilder.ToString();
        }

		private static string PadAndFormat(object value, int length) {
			if (value == null) {
				value = String.Empty;
			}
			if (value is string || value is char) {
				return value.ToString().PadRight(length, ' ').Substring(0, length);
			}
			if (value is int || value is long) {
				return value.ToString().PadLeft(length, '0');
			}
			if (value is decimal) {
				decimal d = Math.Truncate(((decimal)value) * 100);
				return d.ToString().PadLeft(length, '0');
			}
			if (value is DateTime) {
				if (((DateTime)value) == DateTime.MinValue) {
					return PadAndFormat(" ", length);
				}
				if (length == 10) {
					return ((DateTime)value).ToString("yyMMddHHmm");
				}
				return ((DateTime)value).ToString("yyMMdd");
			}
			throw new ArgumentException("Object was not of a recognized type.  " + value.GetType().ToString() + " not implemented.");
		}
    }
}
